keyorder
=========

This module offers utility to easily convert between dictionaries and list of parameters,
when the conversion always needs to be done in the same order. It is useful in settings where lists or
arrays of values are expected, where the order has a meaning.

Examples
--------

```python
import numpy as np
# names sets the order of the parameters
names = ['p_0', 'p_1', 'p_2']
kord = KeyOrder(names)
# A user friendly way to give parameter values
# Notice that they do not need to be in the right order
user_friendly_params = {
   'p_0': 0,
   'p_2': 0.2,
   'p_1': 0.1
}
# Order the user friendly dict in a numpy array with
# the right order
np_array_params = kord.to_numpy(user_friendly_params)
(np_array_params == np.array([0, 0.1, 0.2])).all()
```

Instal
------

```shell
pip install --user https://gitlab.inria.fr/InBio/Public/keyorder.git
```

Crude doc
--------

```
 class keyorder.keyorder.KeyOrder(*args, **kwds)

    The class representing a fixed order for conversion.

    KeyOrder(param_name) where param_names is an iterable of KeyType.

    to_dict(src) → Dict[KeyType, ValueType]

    to_list(src) → [~ValueType]

    to_numpy(src) → np.array[ValueType]

    to_series(src) → pd.Series[KeyType, ValueType]
```