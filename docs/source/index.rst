.. keyorder documentation master file, created by
   sphinx-quickstart on Fri Nov  6 15:17:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to keyorder's documentation!
====================================

.. automodule:: keyorder
    :members:
    :undoc-members:


Main clas
---------

.. automodule:: keyorder.keyorder
    :members:
    :undoc-members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

