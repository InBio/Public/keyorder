from typing import TypeVar, Generic, Mapping, Iterable, Dict
import numpy as np
import pandas as pd

__all__ = [
    'KeyOrder',
]

T = TypeVar('KeyType') 
S = TypeVar('ValueType')

class KeyOrder(Generic[T]):
    """The class representing a fixed order for conversion.

    `KeyOrder(param_name)` where param_names is an iterable of *KeyType*.
    """
    def __init__(self, names: Iterable[T]):
        self.names_list = list(names)
        self.index_dict = dict()
        for (i,v) in enumerate(names):
            self.index_dict[v] = i

    def index(self, key: T) -> int:
        return self.index_dict[key]
    
    def name(self, index: int) -> T:
        return self.names_list[index]

    def to_dict(self, src) -> Dict[T,S]:
        if isinstance(src,Mapping):
            return dict(src)
        if isinstance(src,Iterable):
            return dict(zip(self.names_list, src))
    
    def to_list(self, src) -> [S]:
        if isinstance(src,Mapping):
            return [src[n] for n in self.names_list]
        if isinstance(src,Iterable):
            return list(src)
    
    def to_series(self,src) -> "pd.Series[KeyType,ValueType]":
        return pd.Series(self.to_dict(src))
    
    def to_numpy(self,src) -> "np.array[ValueType]":
        return np.array(self.to_list(src))
    
