"""Easily pass from dict-like to list-like."""
import setuptools

with open('Readme.markdown', 'r') as f:
    long_description = f.read()

setuptools.setup(
    version='1.0.0',
    name='keyorder',
    scripts=[],
    author='Arthur Carcano',
    author_email='arthur.carcano@inria.fr',
    description='Easily pass from dict-like to list-like.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    install_requires=[
        'numpy',
        'pandas',
    ],
)