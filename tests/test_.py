from keyorder import KeyOrder
import pandas as pd
import numpy as np

class Test:

    def test_init(self):
        names = [str(i) for i in range(5)]
        kord = KeyOrder(names)
        assert( kord.names_list == names )
        for (k,v) in kord.index_dict.items():
            assert(k == str(v))
    
    def test_dict(self):
        names = [f'p_{i}' for i in range(5)]
        kord = KeyOrder(names)
        params_dict = {f'p_{i}': 17*i + 5 for i in range(5)}
        params_list = [17*i + 5 for i in range(5)]
        assert( kord.to_dict(params_dict) == params_dict )
        assert( kord.to_dict(params_list) == params_dict )
        assert( kord.to_dict(np.array(params_list)) == params_dict )
        assert( kord.to_dict(pd.Series(params_dict)) == params_dict )
    
    def test_list(self):
        names = [f'p_{i}' for i in range(5)]
        kord = KeyOrder(names)
        params_dict = {f'p_{i}': 17*i + 5 for i in range(5)}
        params_list = [17*i + 5 for i in range(5)]
        assert( kord.to_list(params_dict) == params_list )
        assert( kord.to_list(params_list) == params_list )
        assert( kord.to_list(np.array(params_list)) == params_list )
        assert( kord.to_list(pd.Series(params_dict)) == params_list )

    def test_np(self):
        names = [f'p_{i}' for i in range(3)]
        kord = KeyOrder(names)
        assert(
            (kord.to_numpy({'p_2': 2, 'p_0': 0, 'p_1': 1}) == np.array([0,1,2])).all()
        )